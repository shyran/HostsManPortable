# Changelog

## 1.2 (2016/01/30)

* Add hm.ini and update.cfg (convert to update2.cfg) symlinks from AppData folder.

## 1.1 (2016/01/28)

* Forgot hm.cfg symlink.

## 1.0 (2016/01/28)

* Initial version based on HostsMan 4.6.103.
